﻿//POL - Połowa
//Dla podanego ciągu długości 2* k, wypisz na standardowe wyjście dokładnie pierwszą połowę ciągu.

// Wejście
// W pierwszej linijce wejścia znajduje się jedna liczba całkowita t(1<=t<=100). Każdy wiersz o numerze od 2 do t+1, zawiera ciąg długości 2* k(1<=k<=1000).

//Wyjście
//Dla każdego przypadku testowego na wyjściu powinien pojawić się ciąg będący pierwszą połową wczytanego ciągu.

//Example
//Wejście:
//3
//pierwszy
//lubiec
//ktotozrobi

//Output:
//pier
//lub
//ktoto


using System;

namespace Połowa
{
  class Program
  {
	 static void Main(string[] args)
	 {
		string[] rozwiązanie = new string[Convert.ToInt32(Console.ReadLine())];
		for (int i = 0; i <= rozwiązanie.Length - 1; i++)
		{
		  rozwiązanie[i] = PobierzCiag();
		}
		for (int i = 0; i <= rozwiązanie.Length - 1; i++)
		{
		  Console.WriteLine(rozwiązanie[i]);
		}
		Console.ReadKey();

	 }
	 static string PobierzCiag()
	 {
		string rozwiazanie = "";
		string ciag = Console.ReadLine();
		char[] tablica = ciag.ToCharArray();
		for (int i = 0; i <= tablica.Length / 2 - 1; i++)
		{
		  rozwiazanie += tablica[i];
		}
		return rozwiazanie;
	 }
  }
}
