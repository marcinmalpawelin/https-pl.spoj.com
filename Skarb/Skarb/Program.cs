﻿//Zadanie D.Skarbfinder
//Opis
//Pewien bardzo znany poszukiwacz skarbów, pan Skardfinder, właśnie odkrył kolejną w swoim życiu tajemniczą mapę określającą położenie skarbu.Mapa jest bardzo specyficzna, zawiera jedynie szereg liczb, po dwie w wierszu, których jest bardzo dużo. Po wielu miesiącach uważnego studiowania mapy i okoliczności jej zdobycia, pan Skardfinder wreszcie odkrył, co liczby te oznaczają. Otóż każda para jest pojedynczą wskazówką, gdzie pierwsza liczba w wierszu oznacza kierunek, w którym należy iść, a druga ilość kroków, które trzeba wykonać w tym kierunku. Kierunki te są zakodowane następująco:

//        0 - północ
//        1 - południe
//        2 - zachód
//        3 - wschód

//Kolejnym odkryciem było to, że liczenie kroków trzeba rozpocząć od pewnej studni. Teraz nie pozostało nic innego, jak tylko pojechać tam i zacząć chodzić, jak mapa wskazuje, a potem odkopać skarb. Ale...tych wskazówek może być nawet 100000, a kroków w każdej z nich nawet do 10000. Nikt rozsądny oczywiście nie będzie chodził tyle pieszo, a na pewno nie tak znany poszukiwacz skarbów, jak pan Skardfinder.Dał Ci więc zadanie napisania dla niego programu, który wskaże, jak osiągnąć skarb robiąc jak najmniej kroków (oczywiście poruszając się tylko zgodnie z etykietą poszukiwaczy skarbów tzn.tylko w kierunkach północ-południe, lub wschód-zachód), lub powie, że skarb znajduje się we wspomnianej studni(jeśli kroki zaprowadzą nas do punktu, z którego zaczynaliśmy liczyć). Teren, na którym szukamy skarbu jest pustym polem i w zasięgu mapy nie ma na nim żadnych przeszkód, które uniemożliwiałyby robienie kroków w którymkolwiek z kierunków.

//Specyfikacja wejścia
//Pierwsza linia wejścia zawiera liczbę całkowitą D (1 ≤ D ≤ 50), oznaczającą liczbę zestawów danych.Pierwsza linia każdego zestawu zawiera jedną liczbę całkowitą N (0 ≤ N ≤ 100000), oznaczającą liczbę wskazówek - par określających kierunek i ilość kroków do wykonania.Kolejnych N linii składa się z liczb a i b(0 ≤ a ≤ 3; 1 ≤ b ≤ 10000), które oznaczają pojedynczą wskazówkę.

//Specyfikacja wyjścia
//Dla każdego zestawu danych należy wypisać najkrótszą drogę zgodną z zasadami etykiety poszukiwaczy skarbów, lub słowo studnia, jeśli skarb znajduje się w studni. Drogę oznaczamy tak samo jak na mapie, przez pary kierunek i ilość kroków w tym kierunku wypisanych w jednej linii.Jeśli do skarbu prowadzi prosta droga, należy wypisać tylko jedną linię z kierunkiem i ilością kroków. Jeśli droga musi skręcać, to wtedy pan Skardfinder po przeprowadzeniu na miejscu wizji lokalnej prosił, aby najpierw kroki wykonywać w orientacji północ-południe, a dopiero gdy znajdziemy się na odpowiedniej wysokości w tej orientacji, zacząć robić kroki w orientacji wschód-zachód.

//Przykład
//Wejście
//3
//3
//1 1
//0 2
//3 1
//4
//0 1
//2 1
//1 1
//3 1
//2
//0 1
//0 2
//Wyjście
//0 1
//3 1
//studnia
//0 3







using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skarb
{
 
	 class Program
	 {
		static void Main(string[] args)
		{
		  int Ilosctestow = Convert.ToInt32(Console.ReadLine());
		  for (int i = 0; i < Ilosctestow; i++)
		  {
			 GlownaFunkcja();

		  }





		  Console.ReadKey();


		}
		static void GlownaFunkcja()
		{


		  float PozycjaX = 0, PozycjaY = 0;
		  int IloscKomend = Convert.ToInt32(Console.ReadLine());
		  string[] Wejscie = new string[IloscKomend];
		  int[] Kierunek = new int[IloscKomend];
		  int[] IloscKrokow = new int[IloscKomend];

		  for (int i = 0; i < IloscKomend; i++)
		  {
			 Wejscie[i] = Console.ReadLine();
			 Kierunek[i] = Convert.ToInt32(ZwrocLiczby(Wejscie[i])[0]);
			 IloscKrokow[i] = Convert.ToInt32(ZwrocLiczby(Wejscie[i])[1]);


			 switch (Kierunek[i])
			 {
				case 0:
				  PozycjaX += IloscKrokow[i];
				  break;
				case 1:
				  PozycjaX -= IloscKrokow[i];
				  break;
				case 2:
				  PozycjaY += IloscKrokow[i];
				  break;
				case 3:
				  PozycjaY -= IloscKrokow[i];
				  break;
			 }

		  }

		  DotrzyjDoPozycji(PozycjaX, PozycjaY);






		}

		static void DotrzyjDoPozycji(float OśX, float OśY)
		{
		  int Kierunek = 0;
		  float IloscKrokow = 0;
		  if (OśX == 0 && OśY == 0)
		  {
			 Console.WriteLine("studnia");
		  }
		  else  // Jeżeli Skarb nie znajduję sie w studni
		  {
			 if (OśY == 0 || OśX == 0)// Jeżeli Skarb znajduję się na j3ednej z OSi
			 {
				if (OśY == 0)
				{
				  if (OśX >= 0)
				  {
					 Kierunek = 0;
					 IloscKrokow = OśX;

				  }
				  else
				  {
					 Kierunek = 1;
					 IloscKrokow = OśX * -1;

				  }

				}
				if (OśX == 0)
				{
				  if (OśY >= 0)
				  {
					 Kierunek = 2;
					 IloscKrokow = OśY;

				  }
				  else
				  {
					 Kierunek = 3;
					 IloscKrokow = OśY * -1;

				  }

				}
				Console.WriteLine(Kierunek + " " + IloscKrokow);

			 }




			 if (OśX != 0 && OśY != 0)// Jeżeli Skarb nie znajduję się na osi
			 {
				string PierwszaKomenda;
				string DrugaKomenda;

				if (OśX > 0)
				{
				  Kierunek = 0;
				  IloscKrokow = OśX;
				}
				else
				{
				  Kierunek = 1;
				  IloscKrokow = OśX * -1;

				}
				PierwszaKomenda = Kierunek + " " + IloscKrokow;
				if (OśY > 0)
				{
				  Kierunek = 2;
				  IloscKrokow = OśY;

				}
				else
				{
				  Kierunek = 3;
				  IloscKrokow = OśY * -1;

				}
				DrugaKomenda = Kierunek + " " + IloscKrokow;

				Console.WriteLine(PierwszaKomenda);
				Console.WriteLine(DrugaKomenda);

			 }

		  }





		}
		static string[] ZwrocLiczby(string Wejscie)
		{
		  char[] Tablica = Wejscie.ToCharArray();
		  string[] Wyniki = new string[2];
		  int licznik = 0;
		  foreach (var item in Tablica)
		  {
			 if (item != ' ')
			 {
				Wyniki[licznik] += item;
			 }
			 else
			 {
				licznik++;
			 }
		  }
		  return Wyniki;

		}



	 }
  
}
