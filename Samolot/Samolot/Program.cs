﻿//Bajtockie Linie Lotnicze wzbogaciły swoją flotę o nowy model samolotu.W samolocie tym jest n1 rzędów miejsc siedzących w klasie biznesowej oraz n2 rzędów w klasie ekonomicznej. W klasie biznesowej każdy rząd ma k1 miejsc siedzących, a w klasie ekonomicznej — k2 miejsc.

//Zadanie
//Napisz program, który:
//wczyta informacje na temat dostępnych miejsc siedzących w samolocie,
//wyznaczy sumaryczną liczbę wszystkich miejsc siedzących,
//wypisze wynik
//Wejście
//W pierwszym i jedynym wierszu wejścia znajdują się cztery liczby naturalne n1, k1, n2, i k2 (1<=n1, k1, n2, k2<=1000), pooddzielane pojedynczymi odstępami.

//	Wyjście
//	Pierwszy i jedyny wiersz wyjścia powinien zawierać jedną liczbę całkowitą - liczbę miejsc siedzących w analizowanym samolocie.

//	Przykład2
//	Wejście
//	2 5 3 20


//	Wyjście
//	70



using System;


namespace Samolot
{
  class Program
  {
	 static void Main()
	 {
		string wejscie = Console.ReadLine();
		wejscie.ToCharArray();
		string puste = " ";
		int wynik;
		string[] n = new string[4];
		n[0] = "";
		n[1] = "";
		n[2] = "";
		n[3] = "";
		int numer = 0;


		for (int i = 0; i <= wejscie.Length - 1; i++)
		{
		  if (wejscie[i].ToString() != puste)
		  {
			 n[numer] += wejscie[i];
		  }
		  else
		  {
			 numer++;

		  }

		}
		int liczba1 = Convert.ToInt16(n[0]);
		int liczba2 = Convert.ToInt16(n[1]);
		int liczba3 = Convert.ToInt16(n[2]);
		int liczba4 = Convert.ToInt16(n[3]);
		wynik = liczba1 * liczba2 + liczba3 * liczba4;

		Console.WriteLine(wynik);


		Console.ReadKey();
	 }

  }
}
