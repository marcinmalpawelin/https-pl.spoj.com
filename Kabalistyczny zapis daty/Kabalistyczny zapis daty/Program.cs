﻿//Istnieje bardzo łatwy sposób zapisu daty.Ten typowo barokowy pomysł nawiązywał do kabały, w której literom hebrajskim przypisane były liczby.W tym wypadku litery alfabetu łacińskiego odpowiadały następującym liczbom:

//A B C D E F G H I K
//1 2 3 4 5 6 7 8 9 10
//L  M   N   O	  P   Q   R   S   T    V 
//20 30  40  50  60  70  80  90  100  200
//X       Y       Z
//300     400     500
//Datę oblicza się sumując wszystkie liczby odpowiadające kolejnym literom tekstu. Zapis stosowano w drukach i rękopisach. W przypadku druków najczęściej podawano pod poszczególnymi słowami sumę liczb ich liter. Autorzy trudzili się nad stworzeniem tekstu, z którego daje się odczytać datę.

//Input
//Na wejściu podany jest wyraz, pisany małymi literami (używając wyłącznie liter podanych powyżej). Wyraz nie większy niż 25 znaków.

//Output
//Na wyjściu podany jest rok w postaci liczby naturalnej, który zapisałeś za pomocą wyrazu(czyli sumy każdej z liczb).

//Example 1
//Input:
//info

//Output:
//105
//Example 2
//Input:
//miska

//Output:
//140



using System;


namespace Kabalistyczny_zapis_daty
{
  class Program
  {
	 static void Main(string[] args)
	 {
		char[] wejscie = Console.ReadLine().ToCharArray();
		int wynik = 0;

		for (int i = 0; i < wejscie.Length; i++)
		{
		  switch (wejscie[i])
		  {
			 case 'a':
				wynik += 1;
				break;
			 case 'b':
				wynik += 2;
				break;
			 case 'c':
				wynik += 3;
				break;
			 case 'd':
				wynik += 4;
				break;
			 case 'e':
				wynik += 5;
				break;
			 case 'f':
				wynik += 6;
				break;
			 case 'g':
				wynik += 7;
				break;
			 case 'h':
				wynik += 8;
				break;
			 case 'i':
				wynik += 9;
				break;
			 case 'k':
				wynik += 10;
				break;
			 case 'l':
				wynik += 20;
				break;
			 case 'm':
				wynik += 30;
				break;
			 case 'n':
				wynik += 40;
				break;
			 case 'o':
				wynik += 50;
				break;
			 case 'p':
				wynik += 60;
				break;
			 case 'q':
				wynik += 70;
				break;
			 case 'r':
				wynik += 80;
				break;
			 case 's':
				wynik += 90;
				break;
			 case 't':
				wynik += 100;
				break;
			 case 'v':
				wynik += 200;
				break;
			 case 'x':
				wynik += 300;
				break;
			 case 'y':
				wynik += 400;
				break;
			 case 'z':
				wynik += 500;
				break;


		  }
		}
		Console.WriteLine(wynik);
		Console.ReadKey();

	 }
  }
}
