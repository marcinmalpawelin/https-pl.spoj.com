﻿//Koło Młodych Miłośników Łamania Szyfrów pracuje nad odszyfrowaniem pewnego starożytnego manuskryptu.Jądrem systemu ma być samouczący się analizator tekstu, a jego pierwszym modułem ma być "zliczacz liter", którego opracowanie powierzono Tobie.

//Opracuj program ZLI, który:
//- wczyta ze standardowego wejścia tekst do analizy,
//- dla każdej litery obliczy liczbę jej wystąpień w tekście,
//- wypisze wynik na standardowe wyjście.

//Wejście
//W pierwszym wierszu N - liczba wierszy tekstu do analizy (N ≤ 150). W każdym z następujących N wierszy ciąg złożony z maksymalnie 200 znaków spośród małych i wielkich liter alfabetu łacińskiego('a'..'z', 'A'..'Z') oraz spacji.

//Wyjście
//W kolejnych wierszach litery od 'a' do 'z' i od 'A' do 'Z' w tej kolejności, a po każdej literze spacja i liczba wskazująca, ile razy ta litera wystąpiła w tekście.

//Uwaga: Pomiń litery, które nie występują w tekście.

//Przykład
//Wejście:
//2
//ala ma kota
//Ola ma psa


//Wyjście:
//a 7
//k 1
//l 2
//m 2
//o 1
//p 1
//s 1
//t 1
//O 1



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zliczacz_liter
{
  class Program
  {

	 static void Main()
	 {
		Litera litera = new Litera();

		int iloscTekstow = Convert.ToInt32(Console.ReadLine());
		string Teskt = "";

		for (int i = 0; i < iloscTekstow; i++)
		{
		  Teskt += Console.ReadLine();
		}

		char[] Wejscie = Teskt.ToCharArray();

		foreach (var item in Wejscie)
		{
		  PoczliczLitere(item, litera);
		}

		for (int i = 0; i < litera.TablicaZnkaow.Length; i++)
		{
		  if (litera.IloscPowtorzenZnaku[i] > 0)
		  {
			 Console.WriteLine(litera.TablicaZnkaow[i] + " " + litera.IloscPowtorzenZnaku[i]);

		  }
		}
		Console.ReadKey();
	 }
	 static void PoczliczLitere(char Litera, Litera Obiekt)
	 {

		for (int i = 0; i < 52; i++)
		{
		  if (Litera == Obiekt.TablicaZnkaow[i])
		  {
			 Obiekt.IloscPowtorzenZnaku[i]++;

			 i = 53;
		  }
		}
	 }
  }
  class Litera
  {
	 public char[] TablicaZnkaow = new char[52];
	 public int[] IloscPowtorzenZnaku = new int[52];

	 char litera = 'a';
	 char duzalitera = 'A';

	 bool duze = false;

	 public Litera()
	 {
		for (int i = 0; i < 52; i++)
		{

		  if (duze == false)
		  {
			 //   Console.WriteLine(litera);

			 TablicaZnkaow[i] = litera;
			 if (litera == 'z')
			 {
				duze = true;
			 }
			 litera++;

		  }
		  else
		  {
			 // Console.WriteLine(duzalitera);

			 TablicaZnkaow[i] = duzalitera;
			 duzalitera++;
		  }

		}

	 }



  }
}
