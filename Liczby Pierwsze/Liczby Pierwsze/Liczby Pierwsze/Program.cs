﻿//Sprawdź, które spośród danych liczb są liczbami pierwszymi

//Input
//n - liczba testów n<100000, w kolejnych liniach n liczb z przedziału[1..10000]

//Output
//Dla każdej liczby słowo TAK, jeśli liczba ta jest pierwsza, słowo: NIE, w przeciwnym wypadku.

//Example
//Input:
//3
//11
//1
//4

//Output:
//TAK
//NIE
//NIE
using System;


namespace Liczby_Pierwsze
{
  class Program
  {
	 static void Main(string[] args)
	 {
		int ilsocliczb = Convert.ToInt32(Console.ReadLine());
		string[] Odp = new string[ilsocliczb];
		for (int i = 0; i <= ilsocliczb - 1; i++)
		{
		  if (Sprawdz(Convert.ToInt32(Console.ReadLine())) == true)
		  {
			 Odp[i] = "TAK";
		  }
		  else
		  {
			 Odp[i] = "NIE";
		  }

		}
		for (int i = 0; i <= ilsocliczb - 1; i++)
		{
		  Console.WriteLine(Odp[i]);
		}
		Console.ReadKey();

	 }
	 static bool Sprawdz(int liczba)
	 {
		int LiczbaDzielnikow = 0;
		if (liczba != 1)
		{
		  for (int i = 1; i <= 10000; i++)
		  {
			 if (liczba % i == 0)
			 {
				LiczbaDzielnikow++;
			 }
		  }
		  if (LiczbaDzielnikow > 2)
		  {
			 return false;
		  }
		  else
		  {
			 return true;
		  }
		}
		else
		{
		  return false;
		}

	 }
  }
}
